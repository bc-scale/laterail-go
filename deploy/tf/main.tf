# Not recommended to deploy and configure K8s resources in the same module
# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs#stacking-with-managed-kubernetes-cluster-resources

# Provision Azure resources, including K8s cluster
module "provision" {
  source = "./provision"

  azure_tenant_id = var.azure_tenant_id
  node_pool       = var.node_pool
  secrets_set     = var.secrets_set
  stage           = var.stage
  gitlab_token    = var.gitlab_token
}

# Bootstrap K8s cluster w/ bare minimum K8s secrets and ArgoCD (w/ ApplicationSets)
module "configure" {
  source = "./configure"

  kube_config     = module.provision.kube_config
  kube_config_raw = module.provision.kube_config_raw
  storage_account = module.provision.storage_account
  secret_service_principal = {
    clientid     = module.provision.azuread_service_principal_appid
    clientsecret = module.provision.azuread_service_principal_password
  }
}

output "azuread_service_principal_objectid" {
  value = module.provision.azuread_service_principal_objectid
}

output "azuread_service_principal_appid" {
  value = module.provision.azuread_service_principal_appid
}