terraform {
  backend "azurerm" {}
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.87"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~>2.7.0"
    }
  }
  required_version = ">= 1.0.0"
}