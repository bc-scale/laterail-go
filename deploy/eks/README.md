# Intro
Aim to replace the current Azure Kubernetes Service based deployment process with EKS clusters
## Requirements
  - Use spot instances, where available
  - Use self managed nodes (support local zones) running Bottlerocket
  - Use [amazon-vpc-cni-k8s](https://github.com/aws/amazon-vpc-cni-k8s) (plugin for pod VPC networking)
  - Avoid [AWS TF EKS Blueprints](https://aws-ia.github.io/terraform-aws-eks-blueprints/main) and EKS addons, where practical
    - Keep process for setting up local/other managed k8s service and EKS as similar as possible
    - Use [upstream TF module](https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest) to make this easier
  - PrivateLink/VPC Peering to MongoDB Atlas
  - Ephemeral. Setup and tear down all resources with each pipeline run
  - Multi-region (eventually)

Good initial references
- [AWS Docs - EKS + Terraform](https://docs.aws.amazon.com/prescriptive-guidance/latest/containers-provision-eks-clusters-terraform/welcome.html)
- [Example Self Managed Node Group](https://github.com/terraform-aws-modules/terraform-aws-eks/tree/master/examples/self_managed_node_group)

# Terraform
## Variable files structure
- Each AWS Account has a variables file in `deploy/tf/eks/variables`
- Each AWS Account may contain multiple EKS clusters. Cluster specific variables and secret variable files are stored under `deploy/tf/eks/variables/<AWS-ACCOUNT-NAME>`
  - E.g. `deploy/tf/eks/variables/dev/eu-west-2.tfvars`

## Deploy
```bash
# Working dir == `deploy/eks` 
export AWS_PROFILE="dwick-dev"
export AWS_REGION="eu-west-2"
export ACCOUNT="dev"
terraform fmt -recursive
terraform init
terraform validate
terraform plan \
  -out ${ACCOUNT}-${AWS_REGION}.tfplan \
  -var-file ./variables/${ACCOUNT}.tfvars \
  -var-file ./variables/${ACCOUNT}/${AWS_REGION}.tfvars \
  -var-file ./variables/${ACCOUNT}/${AWS_REGION}.secret.tfvars
# ~25 mins to deploy
terraform apply ${ACCOUNT}-${AWS_REGION}.tfplan
terraform destroy \
  -var-file ./variables/${ACCOUNT}.tfvars \
  -var-file ./variables/${ACCOUNT}/${AWS_REGION}.tfvars \
  -var-file ./variables/${ACCOUNT}/${AWS_REGION}.secret.tfvars

# Update kubeconfig on workstation
aws eks update-kubeconfig --name ${ACCOUNT}-${AWS_REGION}
```
- See [in-progress testing setup script](./eks.sh) for other setup notes until pipelines are written

# Bottlerocket notes
- Userdata (`module.eks.self_managed_node_groups.bootstrap_extra_args`) needs to be a valid TOML file
  - Quote keys containing `/` e.g. `"node.kubernetes.io/lifecycle"`

# TODO: Ingress and TLS
## Initial Plan:
  - Use aws-load-balancer-controller ingress: ALB, with ACM and WAF
  - Use Ingress Groups, ALB target IP (Pods) etc (see notes below)
  - Investigate how ALB service limits affect deployment

## Secondary investigations:
Use:
  1. Any ingress controller with NLB and CloudFront + WAF
    - Compare with ALB + WAF. Is it worth using NLB when they are available?
  2. aws-load-balancer-controller ingress => nginx/haproxy ingress controller service => app service
  - Use ACM Private CA with cert-manager for TLS on the nginx/haproxy ingress
  - Terminating TLS on the nginx/haproxy ingress
    - Later add some service mesh (e.g. Consul) so Pods use mTLS (with ACM Private CA)
  - Is this possible? -> ALB (TLS, ACM) -> nginx (TLS, cert-manager) -> App
    - Not especially practical. If you need WAF etc then you'd likely just use ALB -> App

## Notes:
  - Only the ALB:
    - can use the WAF directly, unlike Classic and Network Load Balancers
    - is available in Outposts/Local Zones
  - [Beware ALB quotas/limits](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-limits.html)
    - Potentially an issue for clusters with many apps. E.g. path based routing rules, target groups per ALB etc
      - Workaround this by deploying a single ALB which passes all a ingress, which has it's own routing process?
      - Could route traffic from ALB ingress to nginx/haproxy ingress. Examples:
        - [Using an ingress-nginx controller NodePort service](https://github.com/kubernetes-sigs/aws-load-balancer-controller/issues/1384#issuecomment-698591561)
  - [Misc notes from the EKS docs on ALBs](https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html)
    - Use ingress annotation `alb.ingress.kubernetes.io/target-type: ip` to register pods as ALB targets (rather than the nodes)
      - Use with [Pod readiness gate](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/deploy/pod_readiness_gate/) to signal to ALB when Pod is ready for traffic
    - Use Ingress groups to re-use a single ALB for many services
  - [Misc notes from AWS LB Controller docs](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4)
    - [Redirect incoming http -> https](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/guide/tasks/ssl_redirect/)
    - [AWS LB Controller ACM cert discovery](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/guide/ingress/cert_discovery/)
      - Rather than specify the ACM cert ARN on the ingress annotation