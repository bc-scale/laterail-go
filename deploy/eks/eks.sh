#!/bin/bash
<< COMMENT
Deploy Helm charts to Dev EKS cluster. Used during testing, pre-pipeline creation
COMMENT
set -euo pipefail

export AWS_PROFILE="dwick-dev"
export AWS_REGION="eu-west-2"
export ACCOUNT="dev"

# Provision EKS Cluster
terraform fmt -recursive
terraform init
terraform validate
terraform plan \
  -out ${ACCOUNT}-${AWS_REGION}.tfplan \
  -var-file ./variables/${ACCOUNT}.tfvars \
  -var-file ./variables/${ACCOUNT}/${AWS_REGION}.tfvars \
  -var-file ./variables/${ACCOUNT}/${AWS_REGION}.secret.tfvars
terraform apply ${ACCOUNT}-${AWS_REGION}.tfplan

# Update kubeconfig on workstation
aws eks update-kubeconfig --name ${ACCOUNT}-${AWS_REGION}

# Deploy metrics-server, required by HPA
kubectl apply --namespace kube-system \
  -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.6.1/components.yaml

# Create secrets from env vars
for ns in laterail external-dns observability
do
  kubectl create namespace "$ns" 
  kubectl create secret generic azure-secret-sp \
    --from-literal clientid=${SERVICE_PRINCIPAL_CLIENT_ID} \
    --from-literal clientsecret=${SERVICE_PRINCIPAL_CLIENT_SECRET} \
    --namespace "$ns"
done

#Deploy helm charts
helmfile sync \
  --environment test \
  --file ../helmfile/helmfile.yaml

# Delete resources
read -rsp $'Press any key to delete AWS resources\n' -n1 key
helmfile destroy \
  --environment test \
  --file ../helmfile/helmfile.yaml

rm ${ACCOUNT}-${AWS_REGION}.tfplan
terraform destroy \
  -var-file ./variables/${ACCOUNT}.tfvars \
  -var-file ./variables/${ACCOUNT}/${AWS_REGION}.tfvars \
  -var-file ./variables/${ACCOUNT}/${AWS_REGION}.secret.tfvars \
  -force