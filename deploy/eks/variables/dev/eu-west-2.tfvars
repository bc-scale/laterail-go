cluster_context    = "eu-west-2"
kubernetes_version = "1.23"
vpc_cidr           = "10.0.0.0/16"
public_dns = {
  cloudflare_api_token      = "cloudflare"
  domain_name               = "test.laterail.com"
  subject_alternative_names = ["*.test.laterail.com"]
  # zone_id                   = "5fb778e7f6ff310fed3369563be05d77"
}