#!/bin/bash
<< COMMENT
Create minikube cluster for local testing
COMMENT
set -euo pipefail

# New minikube cluster and context
minikube start \
  --nodes 3 \
  --mount-string="/home/alex/git/laterail-go/deploy/migrations:/mnt/migrations" \
  --mount \
  --addons=metallb \
  --profile minikube
  #--addons=ingress,metallb

# Deploy metrics-server, required by HPA
# Minikube addon requires same patch as latest release so might as well use latest
kubectl apply --namespace kube-system \
  -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.6.1/components.yaml \
  --context minikube
kubectl patch deployment \
  metrics-server \
  --namespace kube-system \
  --type='json' \
  -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/args", "value": [
  "--cert-dir=/tmp",
  "--secure-port=4443",
  "--kubelet-insecure-tls",
  "--kubelet-preferred-address-types=InternalIP",
  "--kubelet-use-node-status-port",
  "--metric-resolution=15s"
]}]'

# Setup argocd w/ ApplicationSet controller
# kubectl create namespace argocd --context minikube
# kubectl apply --namespace argocd \
#   -f https://raw.githubusercontent.com/argoproj-labs/applicationset/v0.2.0/manifests/install-with-argo-cd.yaml \
#   --context minikube
# kubectl wait --for=condition=Ready pods --all --namespace argocd --timeout=300s --context minikube
# kubectl port-forward svc/argocd-server --namespace argocd 8080:443 --context minikube &> /dev/null &

# Configure Metallb
# Using a subset of the minikube bridge network adaptor range (e.g. 172.19.0.1/24)
KUBE_SUBNET=$(docker network inspect minikube | jq -r '.[].IPAM.Config[0].Subnet' | sed 's/\.0\/24//g')
cat <<EOF | kubectl apply --context minikube -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: config
  namespace: metallb-system
data:
  config: |
      address-pools:
      - name: default
        protocol: layer2
        addresses:
        - ${KUBE_SUBNET}.100-${KUBE_SUBNET}.250
EOF

# Create secrets from env vars
for ns in laterail external-dns observability
do
  kubectl create namespace "$ns" --context minikube
  kubectl create secret generic azure-secret-sp \
    --from-literal clientid=${SERVICE_PRINCIPAL_CLIENT_ID} \
    --from-literal clientsecret=${SERVICE_PRINCIPAL_CLIENT_SECRET} \
    --namespace "$ns" \
    --context minikube
done

# Wait for argocd secret
# until kubectl --namespace argocd get secret --context minikube | grep "argocd-initial-admin-secret"; do : ; done
# ARGO_PASS=$(kubectl --namespace argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)

# Assign IP to argocd service
# kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}' --context minikube

# Deploy ApplicationSet
# kubectl apply -n argocd -f ../k8s/root.yaml --context minikube
# echo $ARGO_PASS

helmfile sync \
  --environment dev \
  --file ../helmfile/helmfile.yaml