#!/bin/bash
<< COMMENT
Create KinD cluster for local testing
COMMENT
set -e

# Add cluster
cat <<EOF | kind create cluster --wait 30s --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role:  worker
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraMounts:
  - hostPath: /home/alex/git/laterail-go/deploy/migrations
    containerPath: /mnt/migrations
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP
  - containerPort: 443
    hostPort: 443
    protocol: TCP
- role:  worker
  extraMounts:
  - hostPath: /home/alex/git/laterail-go/deploy/migrations
    containerPath: /mnt/migrations
- role:  worker
  extraMounts:
  - hostPath: /home/alex/git/laterail-go/deploy/migrations
    containerPath: /mnt/migrations
EOF

# # Setup argocd w/ ApplicationSet controller
# kubectl create namespace argocd --context kind-kind
# kubectl apply --namespace argocd \
#   -f https://raw.githubusercontent.com/argoproj-labs/applicationset/v0.2.0/manifests/install-with-argo-cd.yaml \
#   --context kind-kind
# kubectl wait --for=condition=Ready pods --all --namespace argocd --timeout=300s --context kind-kind
# kubectl port-forward svc/argocd-server --namespace argocd 8080:443 --context kind-kind &> /dev/null &

# MetalLB install
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.5/config/manifests/metallb-native.yaml --context kind-kind
kubectl wait --for=condition=Ready pods --all --namespace metallb-system --timeout=300s --context kind-kind

# Configure using a subset of the kind bridge network adaptor range (e.g. 172.19.0.1/16)
# Get current range: docker network inspect -f '{{.IPAM.Config}}' kind
KIND_SUBNET=$(docker network inspect kind | jq -r '.[].IPAM.Config[0].Subnet' | sed 's/\.0\.0\/16//g')
cat <<EOF | kubectl apply --context kind-kind -f -
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: laterail
  namespace: metallb-system
spec:
  addresses:
  - ${KIND_SUBNET}.255.100-${KIND_SUBNET}.255.250
---
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: laterail
  namespace: metallb-system
EOF

# Deploy metrics-server, required by HPA
kubectl apply --namespace kube-system \
  -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.6.1/components.yaml \
  --context kind-kind
kubectl patch deployment \
  metrics-server \
  --namespace kube-system \
  --type='json' \
  -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/args", "value": [
  "--cert-dir=/tmp",
  "--secure-port=4443",
  "--kubelet-insecure-tls",
  "--kubelet-preferred-address-types=InternalIP",
  "--kubelet-use-node-status-port",
  "--metric-resolution=15s"
]}]'

# Create secrets from env vars
for ns in laterail external-dns observability
do
  kubectl create namespace "$ns" --context kind-kind
  kubectl create secret generic azure-secret-sp \
    --from-literal clientid=${SERVICE_PRINCIPAL_CLIENT_ID} \
    --from-literal clientsecret=${SERVICE_PRINCIPAL_CLIENT_SECRET} \
    --namespace "$ns" \
    --context kind-kind
  # kubectl create secret generic azure-storage-secret \
  #   --from-literal azurestorageaccountname=${STORAGE_NAME} \
  #   --from-literal azurestorageaccountkey=${STORAGE_KEY} \
  #   --namespace "$ns" \
  #   --context kind-kind
done

# # Wait for argocd secret
# until kubectl --namespace argocd get secret --context kind-kind | grep "argocd-initial-admin-secret"; do : ; done
# ARGO_PASS=$(kubectl --namespace argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)

# # Assign IP to argocd service
# kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}' --context kind-kind

# # Apply dev stage Kustomization
# sed -i "s|REPLACE_STAGE|dev|g" ../k8s/root.yaml
# sed -i "s|REPLACE_REF|$(git branch --show-current)|g" ../k8s/root.yaml
# sed -i "s|REPLACE_API|dev|g" ../k8s/root.yaml
# sed -i "s|REPLACE_WEB|dev|g" ../k8s/root.yaml

# # Deploy ApplicationSet
# kubectl apply -n argocd -f ../k8s/root.yaml --context kind-kind
# echo $ARGO_PASS

helmfile sync \
  --environment dev \
  --file ../helmfile/helmfile.yaml