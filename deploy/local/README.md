# Summary

Resources for local deployment. Includes:

- Kind deployment: Full k8s deployment using local workstation
- docker-compose: Deploy mongodb, use when running `/src/api/main.go` directly

# Kind deployment

- Create and source `/deploy/local/.env` using `/deploy/local/example-env` as a reference
- Run `deploy/local/build.sh`. Updating the `dev` container image tags
- Run `/deploy/local/kind.sh`. Deploys the kind resources, bootstraps ArgoCD and adds public DNS records in CloudFlare, with private IPv4 values to local services
- Access ArgoCD on [https://localhost:8080](https://localhost:8080). Username: `admin`, Password: Final output line of `/deploy/local/kind.sh`
- Access Golang API component at [https://api.dev.laterail.com/](https://api.dev.laterail.com/), React component at [https://dev.laterail.com/](https://dev.laterail.com/)
- Destroy kind cluster with `kind delete cluster`

Notes:

- `/deploy/local/kind.sh` will replace values in `deploy/k8s/root.yaml`, just like the gitlab-ci managed deployments. Don't commit these changes to the repo.

## Quick reference

```
#PWD=deploy/local
set -a && . .env && set +a
./build.sh
./kind.sh
```

# Docker compose deployment

- Create and source `/deploy/local/.env` using `/deploy/local/example-env` as a reference
  - Special consideration to service URLs which will have different DNS addresses (equivalent to container name) in the docker compose network
- Ensure required paths exist for volume mounts
- Deploy with `docker-compose up -d`. Will create and seed a mongodb instance
- Run `src/api/main.go`
- Destroy with `docker-compose up -d`. Will not remove volume mount path resources

# Local AgroCD

By default ArgoCD Application Sets are configured to continuously pull config from the remote repo origin. Disable this for local development investigating/troubleshooting by:

- set `ApplicationSet.spec.template.spec.syncPolicy.automated.selfHeal` == false in `deploy/k8s/root.yaml`
- use the argocd cli to update applications to sync from file paths local to the cli

Example:

```bash
#$PWD=./laterail-go/deploy/local
ARGO_PASS=$(kubectl --namespace argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)
argocd login localhost:8080 --username admin --password $ARGO_PASS --insecure
kubectl -n argocd patch --type='merge' applicationset laterail-core-dev-47-k8s-provisioning-and-deployment-changes -p "{\"spec\":{\"template\":{\"spec\":{\"syncPolicy\":null}}}}"
kubectl -n argocd patch --type='merge' app dependencies -p "{\"spec\":{\"template\":{\"spec\":{\"syncPolicy\":null}}}}"
kubectl -n argocd patch --type='merge' applicationset laterail-dev-47-k8s-provisioning-and-deployment-changes -p "{\"spec\":{\"template\":{\"spec\":{\"syncPolicy\":null}}}}"

# Update with local config
argocd app diff core --local ../k8s/appSet/core/
argocd app diff dependencies --local ../k8s/appSet/dependencies/
argocd app diff dev --local ../k8s/kustomize/overlays/dev

argocd app sync dev --prune --local ../k8s/kustomize/overlays/dev
```
# Helmfile
```bash
# Update lock file
helmfile deps

# Compute resultant manifests
helmfile template -e dev > dev.yaml

# Check for changes
helmfile diff -e dev

# Apply changes
helmfile apply -e dev

# [re]install everything
helmfile sync -e dev
```
# Helm

```bash
helm template ../charts/laterail --debug > test.yaml
helm install laterail -n laterail ../charts/laterail --debug --dry-run
helm install laterail -n laterail ../charts/laterail --debug
helm status laterail --namespace laterail
helm get all laterail --namespace laterail # Show current k8s manifests
```

# kubectl
```bash
kubectl get events -n laterail --sort-by='.metadata.creationTimestamp' -w
kubectl logs --selector app=consumer -n laterail -f
kubectl logs --selector app.kubernetes.io/name=external-dns -n external-dns -f
kubectl top pods -n laterail
```

# Consul Datacenter Outage recovery
- [Consul docs reference](https://developer.hashicorp.com/consul/tutorials/datacenter-operations/recovery-outage#kubernetes-specific-recovery-steps)
```bash
# [FOR TESTING] Stop all server pods to melt consul datacenter quorum
# Might need to do this multiple times
kubectl get pods -l component=server -n consul -o name | 
xargs -I{} kubectl delete {} -n consul

# Get Consul server ID and address from first lines of log
# Applicable if not already rotated
kubectl get pods -l component=server -n consul -o name | 
xargs -I{} kubectl logs {} -n consul --limit-bytes=1024 --since=0

# Create the peers.json file
kubectl get pods -l component=server -n consul -o name | 
xargs -I{} kubectl exec {} -n consul -- /bin/sh -c 'cat <<EOF > /consul/data/raft/peers.json
[
  {
    "id": "198800a3-0253-b43d-fdda-57d63a6e92ef",
    "address": "10.244.1.11:8300",
    "non_voter": false
  },
  {
    "id": "46aff9d2-c725-25fc-3663-acf2d42e7bca",
    "address": "10.244.2.11:8300",
    "non_voter": false
  },
  {
    "id": "26d17084-dbee-a84b-3dc0-d188937ed02c",
    "address": "10.244.3.12:8300",
    "non_voter": false
  }
]
EOF'

# Leave the dc to force k8s to gracefully restart without killing the pod and changing the address
kubectl get pods -l component=server -n consul -o name | 
xargs -I{} kubectl exec {} -n consul -- consul leave

# Check new raft config
kubectl get pods -l component=server -n consul -o name | 
xargs -I{} kubectl exec {} -n consul -- consul operator raft list-peers
```