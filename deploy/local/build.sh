#!/bin/bash
set -e

REPO_ROOT=$PWD

# Build container images
# cd "$REPO_ROOT/../../src/api"
# docker build . -t registry.gitlab.com/alexchadwick/laterail-go/api:dev

cd "$REPO_ROOT/../../src/services"
buildah bud -t registry.gitlab.com/alexchadwick/laterail-go/consumer:alpha-0 -f Containerfile.consumer
buildah bud -t registry.gitlab.com/alexchadwick/laterail-go/producer:alpha -f Containerfile.producer

# cd "$REPO_ROOT/../../react"
# npm install
# npm run build
# docker build . -t registry.gitlab.com/alexchadwick/laterail-go/web:dev

# Push to gitlab
# docker push registry.gitlab.com/alexchadwick/laterail-go/api:dev
# docker push registry.gitlab.com/alexchadwick/laterail-go/web:dev
buildah push registry.gitlab.com/alexchadwick/laterail-go/consumer:alpha-0
buildah push registry.gitlab.com/alexchadwick/laterail-go/producer:alpha