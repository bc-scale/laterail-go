--Database and SQL user app_w created in cockroachdb webapp

BEGIN;

SET sql_safe_updates = FALSE;

USE defaultdb;
DROP DATABASE IF EXISTS laterail CASCADE;
CREATE DATABASE IF NOT EXISTS laterail;

CREATE ROLE write_users;
    GRANT write_users TO app_w;
    ALTER USER app_w CREATEDB;
    GRANT ALL PRIVILEGES ON DATABASE laterail to app_w;

END;