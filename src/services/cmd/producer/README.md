# Producer service
  - accepts and validates initial user request from the web frontend or API
  - assigns a request id (uuid)
  - breaks request down into valid HSP ServiceMetrics requests for external API
  - adds ServiceMetrics request info to message queue, with request id and other metadata
  - returns http 202 success and a request id or an error to request client quickly

Datastore: None

Server library: [Chi](https://github.com/go-chi/chi), incl. [otelchi](https://github.com/riandyrn/otelchi)

# TODO
- Refactor
  - Move re-usable code to the shared `internal` path
- Set production env values for nsq.NewConfig client
  - Move out of queue.New()
- Add request rate limit functionality to this public facing service
  - Existing tollboth package solution from monolith not appropriate for distributed service
  - Possibly replace with rate limiting on the K8s Ingress

# Run

```
laterail-go/src/services$ go run ./cmd/producer

# Requests will add messages to queue. Consume these in testing with:
nsq_to_file --topic=laterail.com.producer --output-dir=/tmp --lookupd-http-address=127.0.0.1:4161
```

## Environmental variables

| name | required | description | example |
|-|-|-|-|
| `API_PORT` | `true` | Port that http service listens on | `8000` |
| `LOG_LEVEL` | `false` | Structured log level | `INFO` |
| `QUEUE_PRODUCER_URL` | `true` | URL for nsqd service  | `<nsqd-url>:4150` |
| `QUEUE_TOPIC_PRODUCER` | `true` | Topic name which producer service pushes messages too | `laterail.com.producer` |
| `TRACE_URL` | `true` | URL of tracing service/collector | `http://otel-collector:14268/api/traces` |

# Example requests
## Submit
```
curl --location --request POST 'http://<SERVICE>/v0/submit' \
--header 'Content-Type: application/json' \
--data-raw '{
    "from_loc":   "BDG",
    "to_loc":     "GLC",
    "from_time":  "0900",
    "to_time":    "1000",
    "from_date":  "2022-09-07",
    "to_date":    "2022-09-07",
    "tolerance": [1, 5, 10]
}'
```