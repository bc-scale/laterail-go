package main

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/riandyrn/otelchi"

	"laterail.com/internal/observability"
	"laterail.com/internal/server"
)

func (app *application) routes() http.Handler {
	r := chi.NewRouter()

	r.Use(middleware.Recoverer)
	r.Use(server.RequestID)
	r.Use(otelchi.Middleware(app.name, otelchi.WithChiRoutes(r)))
	r.Use(observability.ChiMiddlewareLogger)
	r.Use(app.Metrics)
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST"}, //, "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
		// ExposedHeaders:   []string{"Link"},
	}))

	// Initial release
	r.Route("/v0", func(r chi.Router) {
		r.Post("/submit", app.submit)
	})

	r.Handle("/metrics", promhttp.Handler())

	return r
}
