package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"go.opentelemetry.io/otel"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"

	"laterail.com/internal/observability"
	"laterail.com/internal/queue"
)

type application struct {
	ctx     context.Context
	metrics observability.MetricsCollection
	name    string
	queue   queue.Config
}

func main() {
	// Graceful shutdown, context listens for interrupt signal from OS
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	log := observability.Setlevel(os.Getenv("LOG_LEVEL"))

	// OpenTelemetry setup
	serviceName := "producer-service"
	var tp *sdktrace.TracerProvider
	tp, err := observability.InitTracerProvider(os.Getenv("TRACE_URL"), serviceName)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Failed to initialise tracer provider")
	}
	otel.SetTracerProvider(tp)

	// Parent span for main process
	ctx, span := observability.Tracer.Start(ctx, "main")
	defer span.End()
	spanContext := span.SpanContext()

	// Log startup
	log.Debug().
		Time("time", time.Now()).
		Str("trace_id", spanContext.TraceID().String()).
		Str("span_id", spanContext.SpanID().String()).
		Msgf("Starting: %s", serviceName)

	// Setup metrics
	metrics, err := observability.InitMetrics(ctx)
	if err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("failed to initialise metrics")
	}

	// Setup message queue client
	producerURL := os.Getenv("QUEUE_PRODUCER_URL")
	producer, err := queue.New(producerURL, span)
	if err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("failed to initialise message queue producer")
	}

	// Instantiate app
	app := &application{
		ctx:     ctx,
		metrics: metrics,
		name:    serviceName,
		queue: queue.Config{
			ProducerURL: producerURL,
			Topic:       os.Getenv("QUEUE_TOPIC_PRODUCER"),
			Producer:    producer,
		},
	}

	// Initialise server in a goroutine, won't block graceful shutdown handling below
	server := &http.Server{
		Addr:         "0.0.0.0" + ":" + os.Getenv("API_PORT"),
		Handler:      app.routes(),
		IdleTimeout:  time.Minute,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	go func() {
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal().
				Err(err).
				Str("trace_id", spanContext.TraceID().String()).
				Str("span_id", spanContext.SpanID().String()).
				Msg("chi server failed")
		}
	}()

	// Listen for interrupt signal and notify user of shutdown.
	<-ctx.Done()
	stop()
	log.Info().
		Str("trace_id", spanContext.TraceID().String()).
		Str("span_id", spanContext.SpanID().String()).
		Msg("shutting down gracefully, press Ctrl+C again to force")

	// Shutdown server, timeout 5 seconds
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("Server forced to shutdown")
	}

	// End and flush main tracing span before shutdown, timeout 5 seconds
	span.End()
	if err := tp.ForceFlush(ctx); err != nil {
		log.Fatal().
			Err(err).
			Msg("Error flushing tracer spans before shutdown")
	}
	if err := tp.Shutdown(ctx); err != nil {
		log.Fatal().
			Err(err).
			Msg("Error shutting down tracer provider")
	}
}
