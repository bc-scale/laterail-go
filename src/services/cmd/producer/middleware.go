package main

import (
	"net/http"
	"time"

	"github.com/go-chi/chi/v5/middleware"
	"github.com/prometheus/client_golang/prometheus"
)

// Prometheus endpoint metrics
func (app *application) Metrics(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		app.metrics.CounterMap["laterail_endpoint_requests_total"].With(prometheus.Labels{
			"path": r.URL.Path,
		}).Inc()

		// Run other pending handlers then return
		wrappedRw := middleware.NewWrapResponseWriter(w, r.ProtoMajor)
		next.ServeHTTP(wrappedRw, r)

		duration := float64(time.Since(start)) / float64(time.Second)

		app.metrics.HistogramMap["laterail_histogram_request_duration_seconds"].With(prometheus.Labels{
			"path": r.URL.Path,
		}).Observe(duration)

		// Existing summaries require specific input. Referenced by map key
		app.metrics.SummaryMap["laterail_summary_request_duration_seconds"].With(prometheus.Labels{
			"path": r.URL.Path,
		}).Observe(duration)

		app.metrics.SummaryMap["laterail_response_size_bytes"].With(prometheus.Labels{
			"path": r.URL.Path,
		}).Observe(float64(wrappedRw.BytesWritten()))
	})
}
