package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/nsqio/go-nsq"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.opentelemetry.io/otel"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"

	"laterail.com/internal/db"
	"laterail.com/internal/hsp"
	"laterail.com/internal/mongodb"
	"laterail.com/internal/observability"
	"laterail.com/internal/queue"
)

type application struct {
	ctx       context.Context
	hspConfig hsp.HspConfig
	metrics   observability.MetricsCollection
	name      string
	queue     queue.Config
	mongo     *mongo.Client
}

func main() {
	// Graceful shutdown, context listens for interrupt signal from OS
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	log := observability.Setlevel(os.Getenv("LOG_LEVEL"))

	// OpenTelemetry setup
	serviceName := "consumer-service"
	var tp *sdktrace.TracerProvider
	tp, err := observability.InitTracerProvider(os.Getenv("TRACE_URL"), serviceName)
	if err != nil {
		log.Fatal().
			Err(err).
			Msg("Failed to initialise tracer consumer")
	}
	otel.SetTracerProvider(tp)

	// Parent span for main process
	ctx, span := observability.Tracer.Start(ctx, "main")
	defer span.End()
	spanContext := span.SpanContext()

	// Log startup
	log.Debug().
		Time("startup_time", time.Now().UTC()).
		Str("trace_id", spanContext.TraceID().String()).
		Str("span_id", spanContext.SpanID().String()).
		Msgf("Starting: %s", serviceName)

	// Setup metrics
	metrics, err := observability.InitMetrics(ctx)
	if err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("failed to initialise metrics")
	}

	// New queue producer. For pushing HSP ServiceDetails messages
	producerURL := os.Getenv("QUEUE_PRODUCER_URL")
	producer, err := queue.New(producerURL, span)
	if err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("failed to initialise message queue producer")
	}

	// Open psql connection
	if err := db.AddConnection("postgres://" + os.Getenv("DATABASE_URL")); err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("Failed to add new postgres connection")
	}

	// Open mongo connection to collection and seed `stations` collection if empty
	mongo, err := mongodb.NewClient(os.Getenv("MONGO_URI"))
	if err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("Failed to add new mongodb connection")
	}
	stations := mongodb.CollectionModel{
		DB: mongo,
	}
	stations.Coll = stations.GetCollection("stations")
	count, _ := stations.Coll.CountDocuments(context.Background(), bson.D{})
	if count == 0 {
		_, err = stations.AddFromFile(context.Background())
		if err != nil {
			log.Fatal().
				Err(err).
				Str("trace_id", spanContext.TraceID().String()).
				Str("span_id", spanContext.SpanID().String()).
				Msg("Failed to seed new mongodb collection")
		}
	}

	// Instantiate app
	app := &application{
		ctx:     ctx,
		metrics: metrics,
		mongo:   mongo,
		name:    serviceName,
		queue: queue.Config{
			Channel:     os.Getenv("QUEUE_CHANNEL_CONSUMER"),
			Topic:       os.Getenv("QUEUE_TOPIC_PRODUCER"),
			ConsumerURL: os.Getenv("QUEUE_CONSUMER_URL"),
			ProducerURL: producerURL,
			Producer:    producer,
		},
		hspConfig: hsp.HspConfig{
			Username: os.Getenv("HSP_USER"),
			Password: os.Getenv("HSP_SECRET"),
		},
	}

	// New nsq consumer stuff
	config := nsq.NewConfig()
	// Maximum number of times this consumer will attempt to process a message before giving up
	config.MaxAttempts = 10
	// Maximum number of messages to allow in flight
	config.MaxInFlight = 1
	// Maximum duration when Requeueing
	config.MaxRequeueDelay = time.Second * 900
	config.DefaultRequeueDelay = time.Second * 0
	config.MsgTimeout = time.Second * 120
	//Init topic name and channel
	topic := app.queue.Topic
	channel := app.queue.Channel
	//Creating the consumer
	consumer, err := nsq.NewConsumer(topic, channel, config)
	if err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("failed to consume message from queue")
	}
	// Set the Handler for messages received by this Consumer.
	consumer.AddHandler(app)
	//Use nsqlookupd to find nsqd instances
	consumer.ConnectToNSQLookupd(app.queue.ConsumerURL)

	// Initialise server in a goroutine, won't block graceful shutdown handling below
	server := &http.Server{
		Addr:         "0.0.0.0" + ":" + os.Getenv("CONSUMER_PORT"),
		Handler:      app.routes(),
		IdleTimeout:  time.Minute,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	go func() {
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal().
				Err(err).
				Str("trace_id", spanContext.TraceID().String()).
				Str("span_id", spanContext.SpanID().String()).
				Msg("chi server failed")
		}
	}()

	log.Debug().
		Str("trace_id", spanContext.TraceID().String()).
		Str("span_id", spanContext.SpanID().String()).
		Msgf("listening on %s", server.Addr)

	// Listen for interrupt signal and notify user of shutdown.
	<-ctx.Done()
	stop()
	log.Info().
		Str("trace_id", spanContext.TraceID().String()).
		Str("span_id", spanContext.SpanID().String()).
		Msg("shutting down gracefully, press Ctrl+C again to force")

	// Shutdown server and db connections, timeout 5 seconds
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal().
			Err(err).
			Str("trace_id", spanContext.TraceID().String()).
			Str("span_id", spanContext.SpanID().String()).
			Msg("Server forced to shutdown")
	}
	mongo.Disconnect(ctx)
	db.Conn.Close()

	// Stop consumer
	consumer.Stop()

	// End and flush main tracing span before shutdown, timeout 5 seconds
	span.End()
	if err := tp.ForceFlush(ctx); err != nil {
		log.Fatal().
			Err(err).
			Msg("Error flushing tracer spans before shutdown")
	}
	if err := tp.Shutdown(ctx); err != nil {
		log.Fatal().
			Err(err).
			Msg("Error shutting down tracer consumer")
	}
}
