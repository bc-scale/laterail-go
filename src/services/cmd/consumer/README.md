# Consumer service

Two features, sharing a MongoDB collection
- Submitter:
  - stores all request progress and results in MongoDB collection, shared with Tracker endpoint
  - accepts metrics items from queue and submits HSP ServiceMetrics requests
    - waits for response and checks db for existing details
    - adds new detail rids to queue (with request info)
  - accepts details items from queue and submits HSP ServiceDetails requests
    - waits for response and updates database of details and search status
    - marks request id as done, if all service details complete (decrement counter in new db table for requests?)
- Tracker endpoint: 
  - listens on endpoint for request id which represents a pending/completed request to Sumbitter
  - returns submitted requests and their results (details items) and metadata (progress etc)
  - Used by frontend to display results
    - frontend could continuously pull from this service while results are coming in and update frontend as new data arrives
    - Long polling http vs websockets?

# Datastores
  - Postgres/cockroachdb: All rail journey data
  - MongoDB: Processed info for frontend client to pull from via API

# TODO
- Submitter:
  - Only store start and destination station details in mongodb history doc? 
    - Much smaller and easier for frontend to consume

  - Check NSQ client configuration. Suitable for production and this service?
    - Fix logging format

  - Move nsq consumer resources to new package?

  - How to check if new message already processed?
    - [message delivery guarantees](https://nsq.io/overview/design.html#message-delivery-guarantees): delivered at least once (must check for duplicates as consumed)
    - Store message_id in nosql with status property (un/started, done, failed)
    - Needs process for requeuing message if processing fails
      - How to handle loss of a service mid-message processing?
    - Do we care? Maybe an enhancement for later

  - Should the app attempt to apply db migrations on start up?
    - Probably not
      - psql: Currently commented out section in main
      - mongodb: Still applied (add db, user and import station seed data)
    - Doesn't make sense for lots of service replicas to attempt db migrations on startup
    - Need replacement process for lovely automated deployment
  - Using a global variable for postgres DB connection
    - `db.Conn` type `*pgxpool.Pool`

# Run

```
laterail-go/src/services$ go run ./cmd/consumer
```

## Environmental variables

| name | required | description | example |
|-|-|-|-|
| `DATABASE_URL` | `true` | URL of relational database service, incl. credentials | `<USER>:<PASSWORD>@free-tier7.aws-eu-west-1.cockroachlabs.cloud:26257/laterail?sslmode=verify-full&sslrootcert=$HOME/.postgresql/root.crt&options=--cluster%3D<CLUSTER_ID>` |
| `HSP_SECRET` | `true` | Password for external HSP API | `secret` |
| `HSP_USER` | `true` | Username for external HSP API | `<email-address>` |
| `LOG_LEVEL` | `false` | Structured log level | `INFO` |
| `MONGO_URI` | `true` | URL of MongoDB service, incl. credentials | `mongodb://<APP_MONGO_USER>:<APP_MONGO_PASS>@<URL>:27017/laterail?authSource=admin` |
| `QUEUE_CHANNEL_CONSUMER` | `true` | Channel name which consumer pulls messages into  | `<nsqd-url>:4150` |
| `QUEUE_CONSUMER_URL` | `true` | URL for nsqlookupd service | `<nsqlookupd-url>:4161` |
| `QUEUE_TOPIC_PRODUCER` | `true` | Topic name which producer service pushes messages too | `laterail.com.producer` |
| `TRACE_URL` | `true` | Destination service for traces | `http://otel-collector:14268/api/traces` |
| `CONSUMER_PORT` | `true` | Port of the exposed service | `80` |

# Example requests
## Tracker
```
curl --location --request POST 'http://<CONSUMER_SERVICE>/v0/track' \
--header 'Content-Type: application/json' \
--data-raw '{
    "request_ids": [
        "178bf31a-f901-4e59-98bb-a40fc75c00e1", 
        "178bf31a-f901-4e59-98bb-a40fc75c00e2", 
        "178bf31a-f901-4e59-98bb-a40fc75c00e3"
    ]
}'
```
## Stations
```
curl --location --request GET 'http://<CONSUMER_SERVICE>/v0/stations'
```

# Notes 
## use of Generics in Submitter
- Incoming message typed by `main.HandleMessage`
  - Unmarshals `[]byte` to `queue.ServiceMetric` and `queue.ServiceDetails`
    - Errors on failure or incomplete results
- `hsp.SubmitHSPRequest` accepts input as `queue.Message.GenericRequest`  
  - A generic type with contraint `hsp.Request` (must implement the `GetType()` method), satisfied by both `hps.MetricsRequest` and `hsp.DetailsRequest`
  - `hsp.SubmitHSPRequest` checks request type using `GetType()` 
- `hsp.GetHspResponse` accepts input as `queue.Message.GenericRequest`
  - Creates a postbody of `[]byte` to submit to the HSP API
  - Returns an `interface{}` containing either `hsp.MetricsResponse` or `hsp.DetailsResponse` to `hsp.SubmitHSPRequest`
- In `hsp.SubmitHSPRequest` continued, cast the `interface{}` of `untypedResponse` to `hsp.MetricsResponse` or `hsp.DetailsResponse` based on type of `hsp.Request`
  - End of type shenanighans 

## MongoDB History collection
- Stores request history, including all processing progress and final results, for frontend client to consume
  - So there's a lot of duplicated data between mongodb and psql, and potentially, between similar requests, in mongodb
  - I think this is an acceptable trade off
- Alternatives:
  - Only store result RIDs in MongoDB history collection
    - Then query psql for these when required