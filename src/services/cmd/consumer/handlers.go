package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/nsqio/go-nsq"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"laterail.com/internal/db"
	"laterail.com/internal/hsp"
	"laterail.com/internal/initial"
	"laterail.com/internal/mongodb"
	"laterail.com/internal/observability"
	"laterail.com/internal/queue"
	"laterail.com/internal/util"
)

// Store history/progress of a request
type historyDocument struct {
	RequestId      string                 `bson:"request_id,omitempty" json:"request_id,omitempty"`
	Metadata       metadata               `bson:"metadata,omitempty" json:"metadata,omitempty"`
	InitialRequest initial.InitialRequest `bson:"initial_request,omitempty" json:"initial_request,omitempty"`
	Details        []db.AddDetailParams   `bson:"details,omitempty" json:"details,omitempty"`
}

type metadata struct {
	Status          string `bson:"status,omitempty" json:"status,omitempty"`
	TimeCreated     string `bson:"time_created"`
	TimeFinished    string `bson:"time_completed"`
	QueriesTotal    int    `bson:"queries_total" json:"queries_total"`
	QueriesComplete int    `bson:"queries_complete" json:"queries_complete"`
	DetailsTotal    int    `bson:"details_count" json:"details_count"`
	DetailsExisting int    `bson:"details_existing" json:"details_existing"`
	DetailsNew      int    `bson:"details_new" json:"details_new"`
}

// HandleMessage implements the go-nsq.Handler interface.
// Returning a non-nil error will automatically send a REQ command to NSQ to re-queue the message.
func (app *application) HandleMessage(m *nsq.Message) error {
	ctx, span := observability.StartSpan(app.ctx)
	defer span.End()
	// Get span and trace IDs to include in logs
	spanCtx := span.SpanContext()
	traceID := spanCtx.TraceID().String()
	spanID := spanCtx.SpanID().String()

	// Unmarshal message into both Metrics and Details types
	metricMessage, err := queue.Unmarshal[queue.ServiceMetric](m.Body)
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to unmarshall request interface to either hsp MetricsRequest: %w", err))
		return err
	}
	detailsMessage, err := queue.Unmarshal[queue.ServiceDetails](m.Body)
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to unmarshall request interface to either hsp DetailsRequest: %w", err))
		return err
	}

	var requestType string
	var historyDoc historyDocument
	errPromMetric := app.metrics.CounterMap["laterail_messages_error_total"]

	// Setup Mongodb resources
	historyMongo := mongodb.InitCollectionModel("history", app.mongo)

	// Metrics Request
	if metricMessage.Request.FromDate != "" && detailsMessage.Request.Rid == "" {
		requestType = metricMessage.Request.GetType()
		requestID := metricMessage.RequestId
		log.Debug().
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Str("request_id", requestID).
			Str("message_id", metricMessage.MessageId).
			Str("message_type", requestType).
			Msg("message accepted from the queue")

		// Create document for request_id if it doesn't already exist
		_, err := historyMongo.Coll.UpdateOne(
			ctx,
			bson.D{
				{Key: "request_id", Value: historyDoc.RequestId},
			},
			bson.D{
				{Key: "$setOnInsert", Value: historyDocument{
					RequestId:      requestID,
					InitialRequest: metricMessage.InitialRequest,
					Metadata: metadata{
						Status:          "created",
						TimeCreated:     time.Now().UTC().Format(time.RFC3339),
						QueriesTotal:    0,
						QueriesComplete: 0,
						DetailsTotal:    0,
						DetailsExisting: 0,
						DetailsNew:      0,
					},
				}},
			},
			options.Update().SetUpsert(true),
		)
		if err != nil {
			// Existing request_id document
			if mongo.IsDuplicateKeyError(err) {
				log.Debug().
					Str("trace_id", traceID).
					Str("span_id", spanID).
					Str("request_id", requestID).
					Str("message_id", metricMessage.MessageId).
					Str("message_type", requestType).
					Msg("found existing request_id in collection.")
			} else {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to upsert request history document: %w", err))
				return err
			}
		} else {
			log.Debug().
				Str("trace_id", traceID).
				Str("span_id", spanID).
				Str("request_id", requestID).
				Str("message_id", metricMessage.MessageId).
				Str("message_type", requestType).
				Msg("inserted new document in history collection")
		}

		// Increment prometheus counter
		app.metrics.CounterMap["laterail_messages_accepted_total"].With(prometheus.Labels{
			"type": requestType,
		}).Inc()

		sortedRidArray, _, err := hsp.SubmitHSPRequest(hsp.GenericRequest[hsp.Request]{Request: metricMessage.Request}, app.hspConfig, hsp.GetHspResponse, ctx)
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to process new metrics request: %w", err))
			errPromMetric.With(prometheus.Labels{"type": requestType}).Inc()
			return err
		}

		// Check if RIDs exist in database details table. Return list of rids not present
		if len(sortedRidArray) > 0 {
			newRid, existingRid, err := hsp.GroupRIDs(sortedRidArray, ctx)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("failed determine which RIDs in HSP ServiceMetrics response are new: %w", err))
				errPromMetric.With(prometheus.Labels{"type": requestType}).Inc()
				return err
			}

			// Get existingRid detials values from datastore
			existingDetails, err := db.New(db.Conn).ListDetailsByRIDs(ctx, existingRid)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to pull details of existing RIDs from datastore: %w", err))
				errPromMetric.With(prometheus.Labels{"type": requestType}).Inc()
				return err
			}

			countExisting := len(existingDetails)
			if countExisting > 0 {
				existingDetailsConverted := []db.AddDetailParams{}
				for _, detail := range existingDetails {
					// Only store in MongoDB if matches start/end station
					if detail.Station == metricMessage.InitialRequest.FromLoc || detail.Station == metricMessage.InitialRequest.ToLoc {
						detailConverted, err := util.ConvertType[db.AddDetailParams](detail)
						if err != nil {
							err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to pull details of existing RIDs from datastore: %w", err))
							errPromMetric.With(prometheus.Labels{"type": requestType}).Inc()
							return err
						}
						existingDetailsConverted = append(existingDetailsConverted, *detailConverted)
					}
				}

				_, err := historyMongo.Coll.UpdateOne(
					ctx,
					bson.D{
						{Key: "request_id", Value: requestID},
					},
					bson.D{
						{Key: "$push", Value: bson.M{"details": bson.M{"$each": existingDetailsConverted}}},
						{Key: "$inc", Value: bson.M{"metadata.details_existing": countExisting}},
						{Key: "$inc", Value: bson.M{"metadata.details_count": countExisting}},
					},
				)
				if err != nil {
					err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to update request history document: %w", err))
					return err
				}
			}

			// Set the total_count in datastore
			count := len(newRid)
			if count > 0 {
				_, err := historyMongo.Coll.UpdateOne(
					ctx,
					bson.D{
						{Key: "request_id", Value: requestID},
					},
					bson.D{
						{Key: "$inc", Value: bson.M{"metadata.queries_total": count}},
					},
				)
				if err != nil {
					err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to update request history document: %w", err))
					return err
				}
			} else if count == 0 {
				// There's no details requests required for this metrics request
				// Value will be overwritten by any successful details requests generated by any other metrics requests created
				// by the parent initial request
				_, err := historyMongo.Coll.UpdateOne(
					ctx,
					bson.D{
						{Key: "request_id", Value: requestID},
					},
					bson.D{
						{Key: "$set", Value: bson.M{"metadata.status": "empty"}},
						{Key: "$set", Value: bson.M{"metadata.time_completed": time.Now().UTC().Format(time.RFC3339)}},
					},
				)
				if err != nil {
					err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to update request history document: %w", err))
					return err
				}
			}

			// Insert RIDs into message queue
			err = app.PushDetailsMessage(newRid, requestID, ctx)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to produce details messages: %w", err))
				errPromMetric.With(prometheus.Labels{"type": requestType}).Inc()
				return err
			}
		}

	} else if metricMessage.Request.FromDate == "" && detailsMessage.Request.Rid != "" {
		// Details Message
		requestType = detailsMessage.Request.GetType()
		requestID := detailsMessage.RequestId
		log.Debug().
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Str("request_id", requestID).
			Str("message_id", detailsMessage.MessageId).
			Str("message_type", requestType).
			Msg("message accepted from the queue")

		app.metrics.CounterMap["laterail_messages_accepted_total"].With(prometheus.Labels{
			"type": requestType,
		}).Inc()

		_, newDetails, err := hsp.SubmitHSPRequest(hsp.GenericRequest[hsp.Request]{Request: detailsMessage.Request}, app.hspConfig, hsp.GetHspResponse, ctx)
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to process new details request: %w", err))
			errPromMetric.With(prometheus.Labels{"type": requestType}).Inc()
			return err
		}

		// FIX ME: Making mongodb request here to estimate if this is the final request
		// Also using this to get the InitialRequest data
		err = historyMongo.Coll.FindOne(ctx, bson.D{{Key: "request_id", Value: requestID}}).Decode(&historyDoc)
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to find existing request history document: %w", err))
			return err
		}

		// Only add journeys which match the start/end stations to MongoDB
		filteredDetails := []db.AddDetailParams{}
		for _, details := range newDetails {
			if details.Station == historyDoc.InitialRequest.FromLoc || details.Station == historyDoc.InitialRequest.ToLoc {
				filteredDetails = append(filteredDetails, details)
			}
		}
		countNew := len(filteredDetails)
		if countNew > 0 {
			historyDoc.Metadata.QueriesComplete = historyDoc.Metadata.QueriesComplete + 1
			updates := bson.D{
				{Key: "$push", Value: bson.M{"details": bson.M{"$each": filteredDetails}}},
				{Key: "$inc", Value: bson.M{"metadata.details_new": countNew}},
				{Key: "$inc", Value: bson.M{"metadata.details_count": countNew}},
				{Key: "$inc", Value: bson.M{"metadata.queries_complete": 1}},
			}

			// Last details request to process
			// FIX ME
			if historyDoc.Metadata.QueriesComplete >= historyDoc.Metadata.QueriesTotal {
				historyDoc.Metadata.Status = "completed"
				updates = append(updates, []bson.E{
					{Key: "$set", Value: bson.M{"metadata.status": "completed"}},
					{Key: "$set", Value: bson.M{"metadata.time_completed": time.Now().UTC().Format(time.RFC3339)}},
				}...)
			}

			_, err := historyMongo.Coll.UpdateOne(
				ctx,
				bson.D{
					{Key: "request_id", Value: requestID},
				},
				updates,
			)
			if err != nil {
				err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to update request history document: %w", err))
				return err
			}
		}
	} else {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to unmarshall request interface to either hsp DetailsRequest or MetricsRequest"))
		errPromMetric.With(prometheus.Labels{"type": requestType}).Inc()
		return err
	}

	app.metrics.CounterMap["laterail_messages_completed_total"].With(prometheus.Labels{
		"type": requestType,
	}).Inc()
	return nil
}

// Produce new messages for HSP Service Details requests
func (app *application) PushDetailsMessage(rid []int64, requestID string, ctx context.Context) error {
	_, span := observability.StartSpan(ctx)
	defer span.End()

	spanCtx := span.SpanContext()
	traceID := spanCtx.TraceID().String()
	spanID := spanCtx.SpanID().String()

	// Add each HSP ServiceDetails request as single message in queue
	for i, req := range rid {
		msg := queue.ServiceDetails{
			RequestId: requestID,
			MessageId: uuid.New().String(),
			Time:      time.Now().UTC().String(),
			Request:   hsp.DetailsRequest{Rid: strconv.Itoa(int(req))},
		}
		//Convert message as []byte
		payload, err := json.Marshal(msg)
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to marshal queue message: %w", err))
			return err
		}
		//Publish the Message
		err = app.queue.Producer.Publish(app.queue.Topic, payload)
		if err != nil {
			err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to publish details message to topic: %w", err))
			return err
		}

		requestType := "hsp.DetailsRequest"
		log.Debug().
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Str("message_type", requestType).
			Msg(fmt.Sprintf("Added message no: %d", i))
		app.metrics.CounterMap["laterail_messages_created_total"].With(prometheus.Labels{
			"type": requestType,
		}).Inc()
	}
	return nil
}

type tracker struct {
	IDs []string `json:"request_ids"`
}

// Get progress and results of existing request IDs
func (app application) tracker(w http.ResponseWriter, r *http.Request) {
	ctx, span := observability.StartSpan(r.Context())
	defer span.End()

	spanCtx := span.SpanContext()
	traceID := spanCtx.TraceID().String()
	spanID := spanCtx.SpanID().String()

	// Decode json POST body
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	var request tracker
	if err := decoder.Decode(&request); err != nil {
		errMsg := "failed to decode POST body"
		observability.NewErrorLogWithTrace(err, span, errMsg)
		http.Error(w, errMsg, http.StatusBadRequest)
		return
	}

	// Setup Mongodb collection wrapper
	historyMongo := mongodb.InitCollectionModel("history", app.mongo)

	// Check the status of given request id
	reponseArray := []historyDocument{}
	for _, id := range request.IDs {
		historyDoc := historyDocument{}
		err := historyMongo.Coll.FindOne(ctx, bson.D{{Key: "request_id", Value: id}}).Decode(&historyDoc)
		if err != nil {
			errMsg := fmt.Sprintf("{\"message\": \"unknown request_id: %s\"}", id)
			observability.NewErrorLogWithTrace(err, span, errMsg)
			http.Error(w, errMsg, http.StatusBadRequest)
			return
		}
		reponseArray = append(reponseArray, historyDoc)
		log.Debug().
			Str("trace_id", traceID).
			Str("span_id", spanID).
			Msg(fmt.Sprintf("Tracked status of requestID: %s", id))
	}

	// Respond to client
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	err := json.NewEncoder(w).Encode(reponseArray)
	if err != nil {
		errMsg := "{\"message\": \"failed to encode completed tracker response\"}"
		observability.NewErrorLogWithTrace(err, span, errMsg)
		http.Error(w, errMsg, http.StatusInternalServerError)
		return
	}
}
