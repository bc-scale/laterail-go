# Services

Attempt to break existing laterail api monolith (`src/monolith`) into \[micro\]services which can be independently scaled.

[branch `epic/46`](https://gitlab.com/alexchadwick/laterail-go/-/tree/epic/46-messaged-based-microservice-for-external-api-requests)

# Message Queue

Uses NSQ as the message queue for these event driven services
  - [message delivery guarantees](https://nsq.io/overview/design.html#message-delivery-guarantees): delivered at least once (must check for duplicates as consumed)
  - [unordered messages](https://nsq.io/overview/features_and_guarantees.html#messages-received-are-un-ordered)
  - [helm chart](https://github.com/nsqio/helm-chart)

# TODO
- Initial services
  - New k8s setup
- Support multiple instances of each service. Key issues:
  - metrics: Per service and total metrics?
  - rate limiting: >5 concurrent HSP API requests

