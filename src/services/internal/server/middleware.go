package server

import (
	"context"
	"net/http"

	"github.com/google/uuid"
)

// Used for context.Context value, requires key with non-primitive type
type ContextKey string

const ContextKeyRequestID ContextKey = "requestID"

// Set UUID request ID for each request
func RequestID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		ctx := context.WithValue(
			r.Context(),
			ContextKeyRequestID,
			uuid.New().String(),
		)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}
