package queue

import (
	"encoding/json"
	"fmt"

	"github.com/nsqio/go-nsq"
	"go.opentelemetry.io/otel/trace"

	"laterail.com/internal/hsp"
	"laterail.com/internal/initial"
	"laterail.com/internal/observability"
)

type Config struct {
	Channel     string
	ConsumerURL string
	Producer    *nsq.Producer
	ProducerURL string
	Topic       string
}

// Messages in the queue service
type ServiceMetric struct {
	RequestId      string                 `json:"request_id"`
	MessageId      string                 `json:"message_id"`
	Time           string                 `json:"time"`
	Request        hsp.MetricsRequest     `json:"request"`
	InitialRequest initial.InitialRequest `json:"initial_request,omitempty"`
}

type ServiceDetails struct {
	RequestId string             `json:"request_id"`
	MessageId string             `json:"message_id"`
	Time      string             `json:"time"`
	Request   hsp.DetailsRequest `json:"request"`
}

type Message struct {
	RequestId      string                          `json:"request_id"`
	MessageId      string                          `json:"message_id"`
	Time           string                          `json:"time"`
	GenericRequest hsp.GenericRequest[hsp.Request] `json:"request"`
}

// Create NSQ Producer
func New(producerURL string, span trace.Span) (*nsq.Producer, error) {
	// Setup NSQ client
	config := nsq.NewConfig()
	//Creating the Producer using NSQD Address
	producer, err := nsq.NewProducer(producerURL, config)
	if err != nil {
		err = observability.NewErrorWithTrace(span, fmt.Errorf("failed to create nsq producer: %w", err))
		return nil, err
	}

	return producer, nil
}

// Unmarshal any type. Used for both ServiceMetric and ServiceDetails messages
func Unmarshal[T any](source []byte) (T, error) {
	var message T

	if err := json.Unmarshal(source, &message); err != nil {
		return message, err
	}

	return message, nil
}
