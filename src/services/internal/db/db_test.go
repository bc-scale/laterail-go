package db

import (
	"context"
	"database/sql"
	"os"
	"testing"

	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

var dbServer string

func TestMain(m *testing.M) {
	dbServer = setDBServer("DB_SERVER", "localhost")

	//Run tests
	code := m.Run()
	os.Exit(code)
}

func setDBServer(env, defaultValue string) string {
	dbServer, ok := os.LookupEnv(env)
	if !ok {
		dbServer = defaultValue
	}
	return dbServer
}

func TestAddStation(t *testing.T) {
	ctx := context.Background()
	NewTestDatabaseWithConfig(t, dbServer)

	got, err := New(Conn).AddStation(ctx, AddStationParams{
		StationID:   "XYZ",
		StationName: sql.NullString{"", false},
	})
	if err != nil {
		t.Fatalf("failed to AddStation: %v", err)
	}

	// var got is assigned successfully entered rows
	want := []Station{
		{
			StationID:   "XYZ",
			StationName: sql.NullString{"", false},
		},
	}
	if got[0] != want[0] {
		t.Fatalf("got %v, want %v", got, want)
	}
}
