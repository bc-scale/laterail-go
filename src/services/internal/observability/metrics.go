package observability

import (
	"context"
	"fmt"

	"github.com/prometheus/client_golang/prometheus"
)

type Metric struct {
	Name       string
	Help       string
	Objectives map[float64]float64
	Buckets    []float64
	Type       string
	Label      string
}

type MetricsCollection struct {
	CounterMap    map[string]*prometheus.CounterVec
	HistogramMap  map[string]*prometheus.HistogramVec
	HistogramList []*prometheus.HistogramVec
	SummaryMap    map[string]*prometheus.SummaryVec
}

var defaultMetrics = []Metric{
	{
		Name:  "laterail_endpoint_requests_total",
		Help:  "Total number of requests processed",
		Type:  "counter",
		Label: "path",
	},
	{
		Name:       "laterail_summary_request_duration_seconds",
		Help:       "Latency for processed requests",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		Type:       "summary",
		Label:      "path",
	},
	{
		Name:    "laterail_histogram_request_duration_seconds",
		Help:    "Latency for processed requests",
		Buckets: []float64{1, 5, 10, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 360},
		Type:    "histogram",
		Label:   "path",
	},
	{
		Name:       "laterail_response_size_bytes",
		Help:       "Size of response for processed requests (bytes)",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		Type:       "summary",
		Label:      "path",
	},
	{
		Name:  "laterail_messages_accepted_total",
		Help:  "Total number of NSQ messages accepted",
		Type:  "counter",
		Label: "type",
	},
	{
		Name:  "laterail_messages_created_total",
		Help:  "Total number of NSQ messages created",
		Type:  "counter",
		Label: "type",
	},
	{
		Name:  "laterail_messages_completed_total",
		Help:  "Total number of NSQ messages completed successfully",
		Type:  "counter",
		Label: "type",
	},
	{
		Name:  "laterail_messages_error_total",
		Help:  "Total number of NSQ messages failed",
		Type:  "counter",
		Label: "type",
	},
}

// Creates and registers new prometheus metrics
func InitMetrics(ctx context.Context) (MetricsCollection, error) {
	_, span := StartSpan(ctx)
	defer span.End()

	var c = map[string]*prometheus.CounterVec{}
	var s = map[string]*prometheus.SummaryVec{}
	var hm = map[string]*prometheus.HistogramVec{}
	var h []*prometheus.HistogramVec

	for _, v := range defaultMetrics {
		if v.Type == "counter" {
			counter := prometheus.NewCounterVec(
				prometheus.CounterOpts{
					Name: v.Name,
					Help: v.Help,
				},
				[]string{v.Label})
			c[v.Name] = counter
			if err := prometheus.Register(counter); err != nil {
				NewErrorLogWithTrace(err, span, fmt.Sprintf("Failed to register metric: %s", v.Name))
				return MetricsCollection{}, err
			}
		} else if v.Type == "summary" {
			summary := prometheus.NewSummaryVec(
				prometheus.SummaryOpts{
					Name:       v.Name,
					Help:       v.Help,
					Objectives: v.Objectives,
				},
				[]string{v.Label})
			s[v.Name] = summary
			if err := prometheus.Register(summary); err != nil {
				NewErrorLogWithTrace(err, span, fmt.Sprintf("Failed to register metric: %s", v.Name))
				return MetricsCollection{}, err
			}
		} else if v.Type == "histogram" {
			histogram := prometheus.NewHistogramVec(
				prometheus.HistogramOpts{
					Name:    v.Name,
					Help:    v.Help,
					Buckets: v.Buckets,
				},
				[]string{v.Label})
			h = append(h, histogram)
			hm[v.Name] = histogram
			if err := prometheus.Register(histogram); err != nil {
				NewErrorLogWithTrace(err, span, fmt.Sprintf("Failed to register metric: %s", v.Name))
				return MetricsCollection{}, err
			}
		} else {
			err := fmt.Errorf("failed to register metric %s", v.Name)
			NewErrorLogWithTrace(err, span, fmt.Sprintf("Unknown Metric type %s", v.Type))
			return MetricsCollection{}, err
		}
	}

	collection := MetricsCollection{
		CounterMap:    c,
		HistogramList: h,
		HistogramMap:  hm,
		SummaryMap:    s,
	}

	return collection, nil
}
