package observability

import (
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi/v5/middleware"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel/trace"

	"laterail.com/internal/server"
)

// Set log levels go gin and zerolog. Defaults to Debug
// Alternative: gin = release, zerolog = Info
func Setlevel(level string) zerolog.Logger {
	log := zerolog.New(os.Stdout)
	if level == "INFO" {
		level = "release"
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	} else {
		level = "debug"
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	return log
}

// Chi request logger, extracts trace and space id from request context
// Dependent on github.com/riandyrn/otelchi which manages these spans
func ChiMiddlewareLogger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		t := time.Now()
		wrappedRw := middleware.NewWrapResponseWriter(rw, r.ProtoMajor)
		next.ServeHTTP(wrappedRw, r)

		latency := float32(time.Since(t).Seconds())
		log.Debug().
			Str("trace_id", trace.SpanFromContext(r.Context()).SpanContext().TraceID().String()).
			Str("span_id", trace.SpanFromContext(r.Context()).SpanContext().SpanID().String()).
			Str("request_id", r.Context().Value(server.ContextKeyRequestID).(string)).
			Str("client_ip", r.RemoteAddr).
			Str("user_agent", r.Header.Get("User-Agent")).
			Str("method", r.Method).
			Str("path", r.URL.Path).
			Float32("latency", latency).
			Int("status", wrappedRw.Status()).
			Str("bytes_in", r.Header.Get("Content-Length")).
			Int("bytes_out", wrappedRw.BytesWritten()).
			Msg("")
	})
}
