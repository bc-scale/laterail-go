package util

import (
	"context"
	"fmt"
	"runtime"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.opentelemetry.io/otel/trace"
)

const (
	tracerKey  = "otel-go-contrib-tracer"
	tracerName = "go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
)

var Tracer = otel.GetTracerProvider().Tracer("gin-api")

type ErrorWithTrace struct {
	TraceID string
	SpanID  string
	Err     error
}

func (e *ErrorWithTrace) Error() string {
	return fmt.Sprint(e.Err)
}

// Wrap errors with trace info and record errors in trace
// Use when bubbling errors up
func NewErrorWithTrace(span trace.Span, err error) *ErrorWithTrace {
	span.RecordError(err)
	span.SetStatus(codes.Error, err.Error())
	spanContext := span.SpanContext()

	return &ErrorWithTrace{
		TraceID: spanContext.TraceID().String(),
		SpanID:  spanContext.SpanID().String(),
		Err:     err,
	}
}

func InitTracerProvider(url string) (*sdktrace.TracerProvider, error) {
	exp, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(url)))
	if err != nil {
		return nil, fmt.Errorf("failed to create Jaeger exporter: %w", err)
	}

	tp := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithBatcher(exp),
		sdktrace.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("laterail-api"),
			attribute.String("environment", "dev"),
			attribute.Int64("ID", 1),
		)),
	)

	return tp, nil
}

// Start new span, using function/method name from runtime.Caller()
func StartSpan(ctx context.Context) (context.Context, trace.Span) {
	pc, _, _, _ := runtime.Caller(1)
	funcName := runtime.FuncForPC(pc)
	return Tracer.Start(ctx, funcName.Name())
}

// Add trace info to err log and error messages to trace
// Use when logging errors
func NewErrorLogWithTrace(err error, span trace.Span, msg string) {
	// Get IDs as string from span context
	spanContext := span.SpanContext()

	// Add error and status code to trace span
	span.RecordError(err)
	span.SetStatus(codes.Error, err.Error())

	log.Error().
		Str("trace_id", spanContext.TraceID().String()).
		Str("span_id", spanContext.SpanID().String()).
		Msg(msg)
}

// Gin Logger, extracts trace and space id from request context
// Dependent on otelgin.Middleware() which manages these spans
func MiddlewareLogger() gin.HandlerFunc {
	return func(c *gin.Context) {
		t := time.Now()
		c.Next()
		latency := float32(time.Since(t).Seconds())

		status := c.Writer.Status()
		log.Debug().
			Str("trace_id", trace.SpanFromContext(c.Request.Context()).SpanContext().TraceID().String()).
			Str("span_id", trace.SpanFromContext(c.Request.Context()).SpanContext().SpanID().String()).
			Str("request_id", c.Request.Header.Get("X-Request-ID")).
			Str("client_ip", c.ClientIP()).
			Str("user_agent", c.Request.UserAgent()).
			Str("method", c.Request.Method).
			Str("path", c.Request.URL.Path).
			Float32("latency", latency).
			Int("status", status).
			Msg("")
	}
}
