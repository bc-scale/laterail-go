# Ongoing work
- [Epic for monolith (on AKS) -> services (on EKS)](https://gitlab.com/alexchadwick/laterail-go/-/issues/46)
  - [K8s provisioning and deployment changes](https://gitlab.com/alexchadwick/laterail-go/-/issues/47)

## Local dev env
- KinD vs Minikube
  - [Minikube requires Consul servers run as root](https://developer.hashicorp.com/consul/tutorials/kubernetes/kubernetes-minikube#create-a-values-file)
    - [Not true for KinD](https://developer.hashicorp.com/consul/tutorials/kubernetes/kubernetes-minikube#create-a-values-file)

## Terraform
- MongoDB Atlas
  - Using free shared instance for dev
    - Cannot use PrivateLink with Shared tier. Needs Public IP allow listing
  - Use serverless for test and prod + PrivateLink
    - Deploy and destroy via TF in pipeline
- Review [CockroachDB Terraform provider Preview](https://github.com/cockroachdb/terraform-provider-cockroach)
  - [Serverless example](https://github.com/cockroachdb/terraform-provider-cockroach/blob/main/examples/workflows/cockroach_serverless_cluster/main.tf)
- Serverless databases should maintain a separate lifecycle from the ephemeral k8s resources

### EKS
- Initial Terraform resource setup
  - ALB Controller
    - Configure ALB healthchecks on pods. [Ref](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/deploy/pod_readiness_gate/)
    - Configure [Ingress Groups](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/guide/ingress/annotations/#ingressgroup)
    - Review related limits and resource usage
  - WAF: All still todo
- Add [AWS IAM Authenticator for Kubernetes](https://github.com/kubernetes-sigs/aws-iam-authenticator)
  - RBAC for IAM identities using `aws-auth` configmap
  - [aws docs](https://docs.aws.amazon.com/eks/latest/userguide/add-user-role.html)

## Helm
- Refactor:
  - Laterail chart templates. Should follow more standard grouping of resources. [Example](https://developer.hashicorp.com/consul/tutorials/microservices/kubernetes-extract-microservice#deployment-manifest)
  - Update LateRail component labels: `app=laterail`, `component=consumer` etc
  - Add env stage specific values to new Consul helm chart
    - Incl. new ingress config for AWS LB controller (ssl passthru and pod annotations)
  - Move NSQ admin and Consul UI behind oauth2-proxy
    - Format so you can add misc ingresses behind oauth2-proxy from chart values
- Jaeger: Remove AllinOne deployment and just use Tempo?
- Sending too many metrics. Rate limiting by Grafana Cloud Prometheus
- [argocd in docs](https://helmfile.readthedocs.io/en/latest/#argocd-integration)

## Helmfile
- Add [aws-load-balancer-controller](https://github.com/kubernetes-sigs/aws-load-balancer-controller) helm chart
  - [Installation guide](https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.4/deploy/installation/)
  - Requires [IAM Roles for ServiceAccount (IRSA)](https://docs.aws.amazon.com/emr/latest/EMR-on-EKS-DevelopmentGuide/setting-up-enable-IAM.html)
- [helmfile contexts example](https://itnext.io/setup-your-kubernetes-cluster-with-helmfile-809828bc0a9f)
- [Renovate automate helmchart updates](https://github.com/roboll/helmfile#integrations)

## ArgoCD
- Setup Argo ApplicationSet(s) for new EKS helm based deployment, as alternative to Helmfile
  - Aim to keep both up to date for now

## Consul
- LateRail has no service to service communication. So value of service mesh is mTLS between ingress and the k8s pods
  - This doesn't apply to AWS Load Balancer controller ingress with ALB, because the TLS termination doesn't happen within the k8s cluster, where a sidecar can manage it
- Go through [this](https://developer.hashicorp.com/consul/docs/k8s)
- Example of [EKS and Consul](https://github.com/aws-quickstart/quickstart-eks-hashicorp-consul)
  - [Nice blog on Consul DNS with core-dns](https://ervikrant06.github.io/consul/kubernetes/Consul-DNS-kubernetes/)
- Use [HCP Consul](https://cloud.hashicorp.com/products/consul) as persistent Consul datacenter?
  - Create a new dc in each k8s cluster and wan join?
    - Does this give you Consul enterprise features like [Advanced Network support](https://developer.hashicorp.com/consul/docs/enterprise#complex-network-topology-support)

## App
- Wed react frontend:
  - Needs to accept 202 and poll Tracker endpoint w/ request_id
  - Use new consumer service `stations` endpoint
- Consumer service:
  - Remove mongodb migration resources. Will be handled outside of app
- Service health endpoints
  - e.g. [`/health/livez|readyz`](https://github.com/hashicorp-demoapp/product-api-go#endpoints)
- Define application service specs for documentation
  - e.g. [good example](https://developer.hashicorp.com/consul/tutorials/microservices/kubernetes-scope-microservice#example-worksheet)
- How to apply user rate limiting for a distributed service? On the ingress or using a shared datastore?
- How to mark a request as complete?
  - Previous used `QueriesTotal-1` (search `// FIX ME`)
  - Notes below
- Potential additional services
  - Follow service: Keep previously searched journey data up to date. Effectively pre-fetching the results for potential future searches

### Consumer service, mongodb actions
- Create empty `historyDoc` var
- If Metrics:
  - If RequestID is missing from `history` collection, create it
  - Add any existing RIDs details
  - Add number of new RIDs
    - If no new RIDs found, set status as Empty and sets TimeFinished
      --> Prematurely sets Status and TimeFinished. Though these will be overwritten by later requests
        - This will complicate frontend features. How to know when request has really finished? 
  - Upsert RequestID document with contents of `historyDoc`
- If Details:
  - Store current state of RequestID document in `historyDoc`
  - Add new RIDs details to `historyDoc`
  - If `historyDoc.Metadata.QueriesComplete` = `historyDoc.Metadata.QueriesTotal-1` (this is the last known query) then change status to Complete and set TimeFinished
    --> Each Metrics request will update the `QueriesComplete` value. Must assume that Details will only run after all Metrics complete. Not necessarily true, messages in queue are unordered
    --> The query in the last message in the queue will not necessarily be the last to finish. Marked as Completed etc prematurely
